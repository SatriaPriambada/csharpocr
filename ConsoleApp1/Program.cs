﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.Util;
using Emgu.CV;
using Emgu.CV.Structure;
using TrafficRecognition;
using System.Drawing;
using System.IO;
using Emgu.CV.OCR;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start Detection");
            OCRDetector ocrObject = new OCRDetector(@"C:\Users\satri\Documents\TA\ta\TrafficRecognition\tessdata");
            Image myBmp = Image.FromFile("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1.jpg");
            byte[] arr;
            using (MemoryStream ms = new MemoryStream())
            {
                myBmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                arr = ms.ToArray();
            }
            Image<Bgr, byte> img = Image<Bgr, byte>.FromRawImageData(arr) ;
            List<Image<Gray, byte>> MarkerPlateList = new List<Image<Gray, byte>>();
            List<Image<Gray, byte>> filteredMarkerPlateList = new List<Image<Gray, byte>>();
            List<MCvBox2D> boxList = new List<MCvBox2D>();
            List<Tesseract.Charactor[]> licenses = new List<Tesseract.Charactor[]>();

            Image<Gray, byte> grayImage = img.Convert<Gray, Byte>();
            Image<Gray, Byte> canny = new Image<Gray, byte>(grayImage.Size);
            
            ocrObject.DetectMarkerPlate(img, MarkerPlateList, filteredMarkerPlateList, boxList);

            Console.WriteLine("Finish OCR");
            Console.ReadLine();
        }

        private void Otsu(Image<Gray, byte> grayImage)
        {
            Image<Gray, Byte> canny = new Image<Gray, byte>(grayImage.Size);
            //Otsu binarization
            Image<Gray, byte> Img_Source_Gray = grayImage.Copy();
            Image<Gray, byte> Img_Egde_Gray = Img_Source_Gray.CopyBlank();
            Image<Gray, byte> Img_SourceSmoothed_Gray = Img_Source_Gray.CopyBlank();
            Image<Gray, byte> Img_Otsu_Gray = grayImage.CopyBlank();

            Img_SourceSmoothed_Gray = Img_Source_Gray.SmoothGaussian(3);
            double CannyAccThresh = CvInvoke.cvThreshold(Img_Egde_Gray.Ptr, Img_Otsu_Gray.Ptr, 0, 255, Emgu.CV.CvEnum.THRESH.CV_THRESH_OTSU | Emgu.CV.CvEnum.THRESH.CV_THRESH_BINARY);
            double CannyThresh = 0.1 * CannyAccThresh;
            Img_Otsu_Gray.Dispose();

            Img_Egde_Gray = Img_SourceSmoothed_Gray.Canny(CannyThresh, CannyAccThresh);
            Img_Egde_Gray.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\bBoxOtsu.jpg");

            CvInvoke.cvCanny(grayImage, canny, 0, 300, 3);
            using (MemStorage storage = new MemStorage())
                for (Contour<Point> contours = canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_TREE, storage); contours != null; contours = contours.HNext)
                {
                    Console.Write("Contours :");
                    foreach (Point p in contours)
                    {
                        Console.WriteLine(p.ToString());
                    }
                    //Contour<Point> currentContour = contours.ApproxPoly(contours.Perimeter * 0.05, storage);
                    CvInvoke.cvDrawContours(canny, contours, new MCvScalar(255), new MCvScalar(255), -1, 1, Emgu.CV.CvEnum.LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                }

            canny.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\bBox.jpg");

        }
    }
}
