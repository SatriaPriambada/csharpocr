﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using Emgu.Util;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Diagnostics;
using Emgu.CV.OCR;
using Emgu.CV.Util;
using System.Threading;

namespace TrafficRecognition
{
    /// <summary>
    /// A Marker plate detector from http://www.emgu.com/wiki/index.php/Marker_Plate_Recognition_in_CSharp
    /// </summary>
    public class OCRDetector : DisposableObject
    {
        private Tesseract _ocr;

        /// <summary>
        /// Create a Marker plate detector
        /// </summary>
        public OCRDetector(String dataPath)
        {
            //You can download more language definition data from
            //http://code.google.com/p/tesseract-ocr/downloads/list
            //Languages supported includes:
            //Dutch, Spanish, German, Italian, French and English
            //create OCR
            _ocr = new Tesseract(dataPath, "eng", Tesseract.OcrEngineMode.OEM_TESSERACT_CUBE_COMBINED);
            _ocr.SetVariable("tessedit_char_whitelist", "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
        }

        /// <summary>
        /// Detect Marker plate from the given image
        /// </summary>
        /// <param name="img">The image to search Marker plate from</param>
        /// <param name="MarkerPlateList">A list of images where the detected Marker plate region is stored</param>
        /// <param name="filteredMarkerPlateList">A list of images where the detected Marker plate region with noise removed is stored</param>
        /// <param name="boxList">A list where the region of Marker plate, defined by an MCvBox2D is stored</param>
        /// <returns>The list of words for each Marker plate</returns>
        public List<Tesseract.Charactor[]> DetectMarkerPlate(Image<Bgr, byte> img, List<Image<Gray, Byte>> MarkerPlateList, List<Image<Gray, Byte>> filteredMarkerPlateList, List<MCvBox2D> boxList)
        {
            //Stopwatch w = Stopwatch.StartNew();
            List<Tesseract.Charactor[]> Markers = new List<Tesseract.Charactor[]>();
            //select green only
            Image<Hsv, Byte> hsv = img.Convert<Hsv, Byte>();

            Image<Gray, Byte>[] channels = hsv.Split();

            //channels[0] is the mask for green between 40-80

            CvInvoke.cvInRangeS(channels[0], new MCvScalar(50), new MCvScalar(80), channels[0]);
            channels[0].Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1green" + ".jpg");
            //Erode and Dilate not needed 
            /*
            CvInvoke.cvDilate(channels[0], channels[1], IntPtr.Zero, 1);
            channels[1].Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1dilate1" + ".jpg");

            CvInvoke.cvErode(channels[1], channels[2], IntPtr.Zero, 1);
            channels[2].Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1erode1" + ".jpg");
            */

            using (Image<Gray, byte> gray = img.Convert<Gray, Byte>())
            using (Image<Gray, Byte> canny = new Image<Gray, byte>(gray.Size))
            using (MemStorage stor = new MemStorage())
            {
                CvInvoke.cvCanny(gray, canny, 0, 300, 3);
                canny.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1canny" + ".jpg");
                Contour<Point> contours = canny.FindContours(Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE, Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_TREE,stor);
                Console.Write("Contours :");
                //foreach (point p in contours)
                //{
                //    console.writeline(p.tostring());
                //}
                /* canny.FindContours(
                 Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                 Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_TREE,
                 stor);*/
                int i = 0;
                for (; contours != null; contours = contours.HNext)
                {
                    CvInvoke.cvDrawContours(img, contours, new MCvScalar(160), new MCvScalar(0), -1, 10, Emgu.CV.CvEnum.LINE_TYPE.EIGHT_CONNECTED, new Point(0, 0));
                    img.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1drawContourCanny" + i + ".jpg");
                    Contour<Point> approxContour = contours.ApproxPoly(contours.Perimeter * 0.025, contours.Storage);

                    gray.Draw(approxContour.BoundingRectangle, new Gray(), 5);
                    gray.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1contour" + i + ".jpg");

                    Console.WriteLine("approx :");
                    foreach (Point p in approxContour)
                    {
                        Console.WriteLine(p.ToString());
                    }
                }
                //Thread.Sleep(3);
                FindMarkerPlate(contours, gray, canny, MarkerPlateList, filteredMarkerPlateList, boxList, Markers);
            }
            //w.Stop();
            return Markers;
        }

        private void FindMarkerPlate(
           Contour<Point> contours, Image<Gray, Byte> gray, Image<Gray, Byte> canny,
           List<Image<Gray, Byte>> MarkerPlateList, List<Image<Gray, Byte>> filteredMarkerPlateList, List<MCvBox2D> boxList,
           List<Tesseract.Charactor[]> Markers)
        {
            gray.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1gray" + ".jpg");
            //Thread.Sleep(10);
            for (; contours != null; contours = contours.HNext)
            {
                Contour<Point> approxContour = contours.ApproxPoly(contours.Perimeter * 0.05, contours.Storage);
                
                if (approxContour.Area > 100 && approxContour.Total == 4)
                {
                    Console.WriteLine("Here");
                    //img.Draw(contours, new Bgr(Color.Red), 1);
                    if (!IsParallelogram(approxContour.ToArray()))
                    {
                        Contour<Point> child = contours.VNext;
                        if (child != null)
                            FindMarkerPlate(child, gray, canny, MarkerPlateList, filteredMarkerPlateList, boxList, Markers);
                        continue;
                    }

                    MCvBox2D box = approxContour.GetMinAreaRect();

                    double whRatio = (double)box.size.Width / box.size.Height;
                    if (!(3.0 < whRatio && whRatio < 8.0))
                    {
                        Contour<Point> child = contours.VNext;
                        if (child != null)
                            FindMarkerPlate(child, gray, canny, MarkerPlateList, filteredMarkerPlateList, boxList, Markers);
                        continue;
                    }

                    Image<Gray, Byte> plate = gray.Copy(box);
                    plate.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1" +"plate" + ".jpg");
                    Image<Gray, Byte> filteredPlate = FilterPlate(plate);
                    filteredPlate.Save("C:\\Users\\satri\\Documents\\TA\\data\\picture\\16Feb\\1" + "filterPlate" + ".jpg");

                    Tesseract.Charactor[] words;
                    StringBuilder strBuilder = new StringBuilder();
                    using (Image<Gray, Byte> tmp = filteredPlate.Clone())
                    {
                        _ocr.Recognize(tmp);
                        words = _ocr.GetCharactors();

                        if (words.Length == 0) continue;

                        for (int i = 0; i < words.Length; i++)
                        {
                            strBuilder.Append(words[i].Text);
                        }
                    }
                    Console.WriteLine(strBuilder.ToString());
                    Markers.Add(words);
                    MarkerPlateList.Add(plate);
                    filteredMarkerPlateList.Add(filteredPlate);
                    boxList.Add(box);
                }
            }
        }

        /// <summary>
        /// Check if the four points forms a parallelogram
        /// </summary>
        /// <param name="pts">The four points that defines a polygon</param>
        /// <returns>True if the four points defines a parallelogram</returns>
        private static bool IsParallelogram(Point[] pts)
        {
            LineSegment2D[] edges = PointCollection.PolyLine(pts, true);

            double diff1 = Math.Abs(edges[0].Length - edges[2].Length);
            double diff2 = Math.Abs(edges[1].Length - edges[3].Length);
            if (diff1 / edges[0].Length <= 0.05 && diff1 / edges[2].Length <= 0.05
               && diff2 / edges[1].Length <= 0.05 && diff2 / edges[3].Length <= 0.05)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Filter the Marker plate to remove noise
        /// </summary>
        /// <param name="plate">The Marker plate image</param>
        /// <returns>Marker plate image without the noise</returns>
        private static Image<Gray, Byte> FilterPlate(Image<Gray, Byte> plate)
        {
            Image<Gray, Byte> thresh = plate.ThresholdBinaryInv(new Gray(120), new Gray(255));

            using (Image<Gray, Byte> plateMask = new Image<Gray, byte>(plate.Size))
            using (Image<Gray, Byte> plateCanny = plate.Canny(100, 50))
            using (MemStorage stor = new MemStorage())
            {
                plateMask.SetValue(255.0);
                for (
                   Contour<Point> contours = plateCanny.FindContours(
                      Emgu.CV.CvEnum.CHAIN_APPROX_METHOD.CV_CHAIN_APPROX_SIMPLE,
                      Emgu.CV.CvEnum.RETR_TYPE.CV_RETR_EXTERNAL,
                      stor);
                   contours != null; contours = contours.HNext)
                {
                    Rectangle rect = contours.BoundingRectangle;
                    if (rect.Height > (plate.Height >> 1))
                    {
                        rect.X -= 1; rect.Y -= 1; rect.Width += 2; rect.Height += 2;
                        rect.Intersect(plate.ROI);

                        plateMask.Draw(rect, new Gray(0.0), -1);
                    }
                }

                thresh.SetValue(0, plateMask);
            }

            thresh._Erode(3);
            thresh._Dilate(3);

            return thresh;
        }

        protected override void DisposeObject()
        {
            _ocr.Dispose();
        }
    }
}
